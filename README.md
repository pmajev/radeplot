# RaDEPlot

## Motivation

When performing Differential Gene Expression (DEG) analysis on multiple samples or clusters, in the case of scRNA-Seq, properly visualizing the results of such Analyses becomes a challenge.



Available and common visualisation approaches include Scatterplots, Volcanoplots and Heatmaps. All these approaches have specific advantages and disadvantages:

- **Scatterplots** are optimized for comparing the expression between two conditions or samples with each other. Therefor they are limited in the number of samples that can be visualised at once. The significance of the observed differences as well can only be visualised via point size or color gradients.
- **Volcanoplots** are optimized for comparing the expression between two conditions or samples with each other while focusing on the relationships between the fold-change of the difference in expression and its calculated p-value. This allows for more fine-grained comparisons but becomes unprecise and unclear if multiple comparisons are visualised in the same plot.
- **Heatmaps** are best used to display preselected genes and show their strength of expression in multiple different samples. This becomes more chaotic with increasing numbers of samples visualised and has no intrinsic capability of representing the significance of the differences observed.



Heatmaps have a very specialised usecase and are good at what they do. RaDEPlots try to integrate the advantages of Scatter- and Volcanoplots and avoid their disadvantages without introducing to many own disadvantages. It therefore uses a scatterplot separated by sample as a basis which is then decorated with appropriate annotation and circularised for better visualisation. RaDEPlots are thereby capable of visualising e.g. All genes that are upregulated in one cluster in a scRNA-Seq Analysis compared to all other clusters, for each cluster, with foldchange, the p-value of the expression change and the topN highly differentially expressed genes, all in one Plot (see Examples).

It is also possible to consider all genes, the downregulated ones included, by splitting the data into two plots, one displaying up-regulated, the other displaying down-regulated genes. For a scRNA-Seq Analysis with 6 clusters, this reduces the number of graphs from 6 different Scatterplots down to two RaDEPlots, while maintaining the possibility to observe global differences between the samples!

## Installation

To install RaDE from GitLab, you will first need to install devtools via install.packages("devtools"), then run:

```R
devtools::install_git(url = "https://gitlab.gwdg.de/pmajev/radeplot")
```

Alternatively, first download the repository from github and run the following in R:

```r
install.packages("/path/to/unzipped/folder/or/zip", 
repos = NULL, 
type = "source")
```


## Usage

The RaDEPlot Package currently consists of two functions. After installation, the help text for these functions can be accessed from within R with

```R
?Seurat_to_RaDE

?RaDEPlot
```



#### Seurat_to_RaDE

This function handles the calculation of DEGs for a Seurat Object and parsing these into a format that suits the main plotting function. Its Parameters are as follows:

- *seurat*: This Parameter takes the Seurat object.
- *test.use*: Which Method should be used for calculating DEGs? This is handed over to Seurats FindMarkers function. Default: "MAST"
- *assay*: From which Seurat assay to draw the input for the DEG Analysis. Default: "RNA"
- *slot*: From which slot in the chosen assay to draw the input for the DEG Analysis. Default: "data"
- *renormalize*: Whether to renormalize the raw data (slot: "counts") before calculating DEGs. Default: FALSE
- *split.by*: This can have three different values. FALSE, only positive markers (up-regulated genes) for each cluster are calculated. "updown", up- and down-regulated markers are calculated. The resulting plot will be split into one containing all up-regulated and one containing all down-regulated genes. "VARIABLE", the name of any meta data column in the Seurat object to split the data into groups by.  This will result in as many Plots as unique factors are defined in the meta data column, showing genes up-regulated in the respective group. CURRENTLY ONLY WORKS WITH MAX. 2 GROUPS.
- l*abel.top.genes*: How many of the genes with top fold-change to label. Default: 5



#### RaDEPlot

This function handles the main plotting and takes the output of Seurat_to_RaDE to create it:

- *rade.object*: This Parameter takes the output object of Seurat_to_RaDE.
- *rect.spacing*: How much the title bars should be above the maximum value in the data. Default: 1
- *jitter*: should the points/genes be jittered on the x-axis. Default: TRUE
- *pval.upper.limit*: What is the upper limit for -log10(p-values) to be included in the color scale. All higher values are squished down to the limit. Default: 50
- *pval.lower.limit*: Reverse of pval.upper.limit. Default: 0
- *x.label/y.label*: labels for the x/y axis. Default: "Cluster"/"log2(FC)"
- *abslogFC*: Use absolute (TRUE) or +/- logFC (FALSE). This is mostly relevant for splitting via "updown". Default: TRUE
- *legend.position*: Where to put the legend. Default: "top"
- *pt.label.size*: Size of the point/gene labels. Default: 4
- *pt.padding*: How much space should be between a point and a label. Default: 0.5
- *pt.label.padding*: How much space should be between two labels. Default: 0.3
- *pt.label.iter*: With how many iterations should the label positioning be optimized. Default: 100
- *make.radial*: Making the Plot radial takes a long time, as all coordinates have to be transferred into a polar coordinate system. So it makes sense to optimise all other parameters before creating the final radial graph. This switch allows that. Default: TRUE

## Examples

### Single Plot

The following code produces a Plot like displayed below:

`rade_object <-Seurat_to_RaDE(seurat=seurat_object,
test.use="MAST",
assay="RNA",
slot="data",
renormalize=TRUE,
split.by=FALSE,
label.top.genes=5)`

`rade_plot <- RaDEPlot(rade.object=rade_object,
rect.spacing=2,
jitter=TRUE,
pval.upper.limit=300,
pval.lower.limit=0,
x.label="Cluster",
y.label="log2(FC)",
legend.position="top",
pt.label.size=4,
pt.padding=0.5,
pt.label.iter=100,
pt.label.padding=0.3,
make.radial=TRUE)`



![](./examples/single.png)

### Split Plot

The following code produces a Plot like displayed below:

`rade_object <-Seurat_to_RaDE(seurat=seurat_object,
test.use="MAST",
assay="RNA",
slot="data",
renormalize=TRUE,
split.by="GROUP",
label.top.genes=5)`

`rade_plot <- RaDEPlot(rade.object=rade_object,
rect.spacing=2,
jitter=TRUE,
pval.upper.limit=200,
pval.lower.limit=0,
x.label="Cluster",
y.label="log2(FC)",
legend.position="top",
pt.label.size=4,
pt.padding=0.5,
pt.label.iter=100,
pt.label.padding=0.3,
make.radial=TRUE)`

![](./examples/split.png)

## Specifications

The RaDe object is a simple list with the following contents:

- rade_df: A dataframe containing the results of the DEG Analysis. The columns present are: 

  - "p_val" contains the calculated p-values.    

  - "avg_logFC" contains the average log fold-change. 

  - "pct.1" contains the percentage of cells in group 1 that express the gene.     

  - "pct.2" analogous to pct.1 for group 2.    

  - "p_val_adj" adjusted p-values. 

  - "cluster" the name of the cluster the entry is for.   

  - "gene" the gene the entry is for.     

  - "labels" whether the gene should be labelled.   

  - "xstart" start of the title bar.    

  - "xend" end of the title bar.      

  - "FC" fold-change        

  - "log10pval" -log10 transformed adjusted p-value. 

  - "abslogFC" absolute value of avg_logFC.  

  - "sig" whether adjusted p-value is below 0.001.

    

  - "group": If the data was split by a variable or updown, then the df will also contain this column indicating the group of the entry

    

- labels: a list of the genes that will be labeled

- jitter: a jitter position object

- rectangle_pos: The positions of the title bars

- max_logFC: the maximum log fold-change in the dataset

- split: logical whether to split or not the plot